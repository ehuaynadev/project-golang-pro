package models

type Detail struct {
	DateIni string `json:"date_ini"`
	DateEnd string `json:"date_end"`
	Group   string `json:"group"`
}

type Deta struct {
	Activities []struct {
		Stage   string `json:"stage"`
		Counter int    `json:"counter"`
	} `json:"activities"`
	ActivitiesByDay []struct {
		Label   string `json:"label"`
		Counter int    `json:"counter"`
	} `json:"activities_by_day"`
	StagesByDay []struct {
		Label              string `json:"label"`
		NoInteresado       int    `json:"no_interesado"`
		NoContactado       int    `json:"no_contactado"`
		OportunidadDeVenta int    `json:"oportunidad_de_venta"`
		NoRegresar         int    `json:"no_regresar"`
		Vendido            int    `json:"vendido"`
		Pendiente          int    `json:"pendiente"`
	} `json:"stages_by_day"`
	Total []struct {
		Label              string  `json:"label"`
		NoInteresado       int     `json:"no_interesado"`
		NoContactado       int     `json:"no_contactado"`
		OportunidadDeVenta int     `json:"oportunidad_de_venta"`
		NoRegresar         int     `json:"no_regresar"`
		Vendido            int     `json:"vendido"`
		Pendiente          int     `json:"pendiente"`
		Ubicado            int     `json:"ubicado"`
		Avg                float64 `json:"avg"`
	} `json:"total"`
}
