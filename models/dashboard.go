package models

type Dasboard struct {
	DateIni        string `json:"date_ini"`
	DateEnd        string `json:"date_end"`
	Limit          int    `json:"limit"`
	MinPenetration int    `json:"min_penetration"`
}

type Dash struct {
	MorePenetrated  Penetration    `json:"more_penetrated"`
	MinusPenetrated Penetration    `json:"minus_penetrated"`
	NotPenetrated   NotPenetration `json:"not_penetrated"`
	GuessesBySite   []GuessBySite  `json:"guesses_by_month"`
}

//
type Penetration []struct {
	Sites    string `json:"sites"`
	Counters float64    `json:"counters"`
	Percentage float64 `json:"percentage"`
	Hp       int    `json:"hp"`
}

type NotPenetration []struct {
	Sites string `json:"sites"`
	Hp    int    `json:"hp"`
}

type GuessBySite struct {
	Site   string `json:"site"`
	Months []struct {
		Month string `json:"month"`
		Data  struct {
			Counter int `json:"counter"`
			HP      int `json:"HP"`
		} `json:"data"`
	} `json:"months"`
}
