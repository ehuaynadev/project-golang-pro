package configuration

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/lib/pq"
)

type configuration struct {
	DBServer   string `json:"db_server"`
	DBPort     int    `json:"db_port"`
	DBName     string `json:"db_name"`
	DBUser     string `json:"db_user"`
	DBPassword string `json:"db_password"`
}

//trae la configuracion del json
func getConfiguration() (configuration, error) {
	config := configuration{}

	//se lee el documento json
	file, err := os.Open("./config.json")
	if err != nil {
		return config, err
	}
	//se cierra el file
	defer file.Close()

	//decoder de file
	decoder := json.NewDecoder(file)
	//llenamos la estructura
	err = decoder.Decode(&config)
	if err != nil {
		return config, err
	}

	return config, err
}

//GORM
func GetGormConnection() *gorm.DB {
	config, err := getConfiguration()
	if err != nil {
		log.Fatal(err)
	}

	dsn := fmt.Sprintf("postgres://%s:%s@%s:%v/%s?sslmode=disable",
		config.DBUser,
		config.DBPassword,
		config.DBServer,
		config.DBPort,
		config.DBName)

	db, err := gorm.Open("postgres", dsn)
	if err != nil {
		log.Fatal(err)
	}

	return db
}

//GetSQLConnection func
func GetSQLConnection() (*sql.DB, error) {
	config, err := getConfiguration()
	if err != nil {
		log.Fatal(err)
	}

	dsn := fmt.Sprintf("postgres://%s:%s@%s:%v/%s?sslmode=disable",
		config.DBUser,
		config.DBPassword,
		config.DBServer,
		config.DBPort,
		config.DBName)

	db, err := sql.Open("postgres", dsn)

	return db, err
}
