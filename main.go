package main

import (
	"./controllers"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func main() {

	r := gin.Default()
	config := cors.DefaultConfig()
	config.AllowOrigins = []string{"*"}
	// config.AllowOrigins = []string{"https://*.potencie.com"}
	config.AllowMethods = []string{"POST", "OPTION"}
	r.Use(cors.New(config))
	r.POST("/dashboard", controllers.Dashbooard)
	r.POST("/detail", controllers.Detail)
	go r.Run(":4006")
	r.RunTLS(":5006", "/etc/ssl/certs/STAR_potencie_com.crt", "/etc/ssl/certs/STAR_potencie_com.pem")
}
