package controllers

import (
	"encoding/json"
	"fmt"
	"time"

	"../configuration"
	"../constans"
	"../models"
	"github.com/gin-gonic/gin"
	//"log"
)

func Dashbooard(c *gin.Context) {
	///se crean las variables
	dash := models.Dash{}
	fechas := models.Dasboard{}
	var d string

	///se vuelca la estructura json
	err := c.Bind(&fechas)
	if err != nil {
		fmt.Println(err)
	}

	///se parsea las fechas que ingresan en la estructura como string
	//fecha de inicio
	fechaI, err := time.Parse("2006-01-02", fechas.DateIni)
	if err != nil {
		c.JSON(406, gin.H{"message": "Estas mandando mal la fecha de inicio"})
	}
	//fecha de fin
	fechaF, err := time.Parse("2006-01-02", fechas.DateEnd)
	if err != nil {
		c.JSON(406, gin.H{"message": "Estas mandando mal la fecha de final"})
	}

	///se valida que la fecha de fin no sea mayor a la de inicio
	if fechaF.YearDay() < fechaI.YearDay() || fechaF.Year() < fechaI.Year() {
		c.JSON(406, gin.H{"message": "No puede ser mayor la fecha de inicio a la de final"})
	}

	if fechas.Limit == 0 {
		fechas.Limit = 15
	}
	///se abre conexion con la base de datos
	db, err := configuration.GetSQLConnection()
	if err != nil {
		defer db.Close()
		fmt.Println(err)
		c.JSON(409, gin.H{"error_code": "database_coneccion_not_found", "message": "No hay conexion con la base de datos"})
	}
	//se prepara la ejecucion de la query
	stmt, err := db.Prepare(constans.Procedimiento)
	if err != nil {
		fmt.Printf("Error al preparar el registro: %s", err)
	}

	/*
		row, err := stmt.Query()
		if err != nil {
			fmt.Printf("Error al crear el registro: %s", err)
		}

		for row.Next() {
			err := row.Scan(
				&d.Usuario,
				&d.Fecha,
			)
			if err != nil {
				fmt.Println("3")
				return err
			}
			prueba = append(prueba, d)
		}
	*/
	//se pasan los datos que requiere la query
	row := stmt.QueryRow(fechas.DateIni, fechas.DateEnd, fechas.Limit, fechas.MinPenetration)
	//se escanea lo que retorna la consulta
	row.Scan(&d)
	fmt.Println(d)
	//se convierte el string en bytes
	bytes := []byte(d)
	//se vuelcan los bytes en la estructura de retorno
	json.Unmarshal(bytes, &dash)

	///se valida los null de retorno (Data inconsistente)
	if (dash.MorePenetrated == nil && dash.MinusPenetrated != nil) || (dash.MorePenetrated != nil && dash.MinusPenetrated == nil) {
		c.JSON(409, gin.H{"error_code": "error_data_inconsistent", "message": "The data found does not have congruence"})
	}

	/*
		user := c.Request().Header.Get("user")
		token := c.Request().Header.Get("apitoken")
	*/

	///se retorna un json construido para retornar
	c.JSON(200, gin.H{"dashboards": dash, "fechas": fechas})
}
