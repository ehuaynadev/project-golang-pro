package controllers

import (
	"encoding/json"
	"fmt"

	"../configuration"
	"../constans"
	"../models"
	"github.com/gin-gonic/gin"
)

func Detail(c *gin.Context) {
	detail := models.Detail{}
	deta := models.Deta{}
	var d string

	err := c.Bind(&detail)
	if err != nil {
		fmt.Println("SE produjo un error al recibir el json")
		c.String(400, "No se pudo recibir el json")
		return
	}

	db, err := configuration.GetSQLConnection()
	if err != nil {
		defer db.Close()
		fmt.Println(err)
		c.JSON(409, gin.H{"error_code": "database_coneccion_not_found", "message": "No hay conexion con la base de datos"})
		return
	}
	fmt.Println(constans.Detail)
	stm, err := db.Prepare(constans.Detail)
	if err != nil {
		fmt.Println(err)
		fmt.Println("No se pudo preparar el registro")
		c.JSON(409, gin.H{"error_code": "database_record_not_prepared", "message": "No se pudo preparar el registro"})
		return
	}

	row := stm.QueryRow(detail.DateIni, detail.DateEnd, detail.Group)
	row.Scan(&d)
	db.Close()
	bytes := []byte(d)
	err = json.Unmarshal(bytes, &deta)
	fmt.Print(d)
	fmt.Print(err)
	if err != nil {
		fmt.Println(err)
		c.JSON(409, gin.H{"error_code": "internal_error", "message": "NO se pudo hacer el unmarshal hacia details"})
		return
	}

	c.JSON(200, deta)
}
